namespace tech_test_payment_api.Enums
{
    public enum EnumStatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,
        Cancelada
    }
}