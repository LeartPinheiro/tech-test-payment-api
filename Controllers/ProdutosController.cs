using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutosController : ControllerBase
    {
        private readonly ApiContext _context;

        public ProdutosController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Listar()
        {
            var produtos = _context.Produtos.ToList();
            return Ok(produtos);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var produto = _context.Produtos.Find(id);
            if (produto == null)
            {
                return NotFound();
            }
            return Ok(produto);
        }
        
        [HttpPost]
        public IActionResult Criar(Produto produto)
        {
            _context.Produtos.Add(produto);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = produto.Id }, produto);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Produto produto)
        {
            var produtoBanco = _context.Produtos.Find(id);
            if (produtoBanco == null)
            {
                return NotFound();
            }
            produtoBanco.Nome = produto.Nome;
            produtoBanco.Preco = produto.Preco;
            produtoBanco.Ativo = produto.Ativo;
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}/ativar")]
        public IActionResult Ativar(int id)
        {
            var produtoBanco = _context.Produtos.Find(id);
            if (produtoBanco == null)
            {
                return NotFound();
            }
            produtoBanco.Ativo = true;
            _context.SaveChanges();
            return NoContent();
        }

        [HttpPut("{id}/desativar")]
        public IActionResult Desativar(int id)
        {
            var produtoBanco = _context.Produtos.Find(id);
            if (produtoBanco == null)
            {
                return NotFound();
            }
            produtoBanco.Ativo = false;
            _context.SaveChanges();
            return NoContent();
        }

    }
}