using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedoresController : ControllerBase
    {

        private readonly ApiContext _context;

        public VendedoresController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Listar()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }
            return Ok(vendedor);
        }
        
        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = vendedor.Id }, vendedor);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);
            if (vendedorBanco == null)
            {
                return NotFound();
            }
            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;
            vendedorBanco.Ativo = vendedor.Ativo;
            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);
        }

        [HttpPut("{id}/ativar")]
        public IActionResult Ativar(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }
            vendedor.Ativo = true;
            _context.Vendedores.Update(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpPut("{id}/desativar")]
        public IActionResult Desativar(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }
            vendedor.Ativo = false;
            _context.Vendedores.Update(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("{id}/vendas")]
        public IActionResult ListarVendas(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }
            var vendas = _context.Vendas.Where(v => v.VendedorId == id).ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }

            return Ok(vendas);
        }

    }
}