using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly ApiContext _context;

        public VendasController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Listar()
        {
            var vendas = _context.Vendas.ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }
            return Ok(vendas);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            return Ok(venda);
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            var vendedor = _context.Vendedores.Find(venda.VendedorId);
            if (vendedor == null)
            {
                return NotFound($"Vendedor ID {venda.VendedorId} não encontrado");
            }
            if (!vendedor.Ativo)
            {
                return BadRequest($"Vendedor ID {venda.VendedorId} não está ativo");
            }
            foreach (var item in venda.Produtos)
            {
                var produto = _context.Produtos.Find(item.ProdutoId);
                if (produto == null)
                {
                    return NotFound($"Produto ID {item.ProdutoId} não encontrado");
                }
                if (!produto.Ativo)
                {
                    return BadRequest($"Produto ID {item.ProdutoId} não está ativo");
                }
                if (item.Quantidade <= 0)
                {
                    return BadRequest($"Quantidade do produto ID {item.ProdutoId} deve ser maior que zero");
                }
                if(item.Valor == 0)
                {
                    item.Valor = produto.Preco * item.Quantidade;
                }
                item.Venda = venda;
                _context.ProdutosVendidos.Add(item);
            }
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}/pagar")]
        public IActionResult Pagar(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            if(venda.Status == EnumStatusVenda.Cancelada)
            {
                return BadRequest("Não é possivel alterar uma venda cancelada.");
            }
            if (venda.Pagar())
            {
                _context.Vendas.Update(venda);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest("Mudança de status inválida");
        }

        [HttpPut("{id}/enviar")]
        public IActionResult Enviar(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            if(venda.Status == EnumStatusVenda.Cancelada)
            {
                return BadRequest("Não é possivel alterar uma venda cancelada.");
            }
            if (venda.Enviar())
            {
                _context.Vendas.Update(venda);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest("Mudança de status inválida");
        }
         [HttpPut("{id}/entregar")]
        public IActionResult Entregar(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            if(venda.Status == EnumStatusVenda.Cancelada)
            {
                return BadRequest("Não é possivel alterar uma venda cancelada.");
            }
            if (venda.Entregar())
            {
                _context.Vendas.Update(venda);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest("Mudança de status inválida");
        }
        [HttpPut("{id}/cancelar")]
        public IActionResult Cancelar(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            if(venda.Status == EnumStatusVenda.Cancelada)
            {
                return BadRequest("Não é possivel alterar uma venda cancelada.");
            }
            if (venda.Cancelar())
            {
                _context.Vendas.Update(venda);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest("Mudança de status inválida");
        }

        [HttpGet("a-pagar")]
        public IActionResult ListarAguardandoPagamento()
        {
            var vendas = _context.Vendas.Where(v => v.Status == EnumStatusVenda.AguardandoPagamento).ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }
            return Ok(vendas);
        }

        [HttpGet("pagas")]
        public IActionResult ListarPagas()
        {
            var vendas = _context.Vendas.Where(v => v.Status == EnumStatusVenda.PagamentoAprovado).ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }
            return Ok(vendas);
        }

        [HttpGet("enviadas")]
        public IActionResult ListarEnviadas()
        {
            var vendas = _context.Vendas.Where(v => v.Status == EnumStatusVenda.EnviadoTransportadora).ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }
            return Ok(vendas);
        }

        [HttpGet("entregues")]
        public IActionResult ListarEntregues()
        {
            var vendas = _context.Vendas.Where(v => v.Status == EnumStatusVenda.Entregue).ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }
            return Ok(vendas);
        }

        [HttpGet("canceladas")]
        public IActionResult ListarCanceladas()
        {
            var vendas = _context.Vendas.Where(v => v.Status == EnumStatusVenda.Cancelada).ToList();
            foreach (var venda in vendas)
            {
                venda.Produtos = _context.ProdutosVendidos.Where(p => p.VendaId == venda.Id).ToArray();
            }
            return Ok(vendas);
        }
    }
}