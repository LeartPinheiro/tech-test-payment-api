using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models
{
    public class ProdutoVendido
    {
        [JsonIgnore]
        public int Id { get; set; }
        
        [JsonIgnore]
        [ForeignKey("ProdutoId")]
        public Produto? Produto { get; set; }
        
        [JsonIgnore]
        public int VendaId { get; set; }
        [JsonIgnore]
        [ForeignKey("VendaId")]
        public Venda? Venda { get; set; }
        
        public int ProdutoId { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}