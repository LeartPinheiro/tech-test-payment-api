using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }

        public EnumStatusVenda Status { get; protected set;}
        public DateTime Data { get; set; }
        public int VendedorId { get; set; }
        [ForeignKey("VendedorId")]

        [JsonIgnore]
        public Vendedor? Vendedor { get; set; }
        [MinLength(1, ErrorMessage = "Venda deve ter pelo menos um produto")]
        public ICollection<ProdutoVendido> Produtos { get; set;}

        public Venda()
        {
            Status = EnumStatusVenda.AguardandoPagamento;
            Data = DateTime.Now;
        }
        public bool Pagar()
        {
            if (Status == EnumStatusVenda.AguardandoPagamento)
            {
                Status = EnumStatusVenda.PagamentoAprovado;
                //Aqui seria chamado um metodo para enviar email para os interessados
                return true;
            }
            return false;
        }

        public bool Enviar()
        {
            if (Status == EnumStatusVenda.PagamentoAprovado)
            {
                //Aqui seria chamado um metodo para enviar email para os interessados
                Status = EnumStatusVenda.EnviadoTransportadora;
                return true;
            }
            return false;
        }

        public bool Entregar()
        {
            if (Status == EnumStatusVenda.EnviadoTransportadora)
            {
                //Aqui seria chamado um metodo para enviar email para os interessados
                Status = EnumStatusVenda.Entregue;
                return true;
            }
            return false;
        }

        public bool Cancelar()
        {
            if (Status == EnumStatusVenda.EnviadoTransportadora)
            {
                //Aqui seria chamado um metodo para enviar email para os interessados
                return false;
            }
            Status = EnumStatusVenda.Cancelada;
            return true;
        }
    }
}